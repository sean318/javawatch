const {execSync, spawn} = require('child_process');
const fs = require('fs');
const path = require('path');
let child;

if(!fs.existsSync(path.resolve(__dirname, 'node_modules'))){
	execSync('npm i', {
		cwd: __dirname,
	}); //auto-install dependencies
}

const clear = require('clear');
const cliSelect = require('cli-select');
const chokidar = require('chokidar');
const chalk = require('chalk');

const projects = fs.readdirSync(path.resolve(__dirname, '..'), {
	withFileTypes: true,
}).filter(d=>d.isDirectory() && d.name !== 'javaWatch').map(d=>d.name);

console.log('Please select the project/exercise you would like to hotreload.');



cliSelect({
    values: projects,
    valueRenderer: (value, selected) => {
        if (selected) {
            return chalk.underline(value);
        }
 
        return value;
    },
}).then(project=>{
	const projectDir = path.resolve(__dirname, '..', project.value);
	const srcDir = path.resolve(projectDir, 'src');
	const classDir = path.resolve(projectDir, 'bin');

	const srcFiles = fs.readdirSync(srcDir).filter(f=>f.match('.java'));

	clear();

	console.log('Please select the file to run.');

	cliSelect({
	    values: srcFiles,
	    valueRenderer: (value, selected) => {
	        if (selected) {
	            return chalk.underline(value);
	        }
	 
	        return value;
	    },
	}).then(srcFile=>{
		watch(srcDir, classDir, srcFile.value);
	})
});


function watch(srcDir, classDir, srcFile){
	console.log('hotreload started');

	chokidar.watch(srcDir).on('change', (changeEvent, changePath) => {
		run(srcDir, classDir, srcFile);
	});
	run(srcDir, classDir, srcFile);
}

function run(srcDir, classDir, srcFile) {
	clear();

	


	
	
	const t = Date.now();
	console.log(`compiling ${srcFile}`);
	try{
		execSync(`javac -cp "${classDir}/../lib/:${classDir}/../lib/\*" -d ${classDir} ${path.resolve(srcDir, srcFile)}`);
	}catch(e){
		console.log('compilation failed');
		return;
	}
	console.log(`compiled in ${Date.now()-t}mS. executing.\n\n\n`);
	if(child){
		child.kill();
	}
	child = spawn('java', ['-cp', `${classDir}/../lib/gui.jar:.`, '-Dapple.awt.UIElement=true', srcFile.replace('.java', '')], {
		cwd: classDir,
		stdio: [
		    0,
		    0,
		],
	});
}