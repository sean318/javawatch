javaWatch is small tool designed to make working with the D-INFK Java exercises more enjoyable for students not wanting to use eclipse.

Installation:
Make sure you have nodeJS and NPM installed. Then simply clone this repo (or download the ZIP), and place it in your exercises folder as follows:

- exercises
	- javaWatch
	- u0
		- src
		- bin
	- u1
		- src
		- bin
	- u2
		- src
		- bin

then you can simply run it using:

`cd exercises`
`node javaWatch`